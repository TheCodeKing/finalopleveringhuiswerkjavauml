package Oef47;

public class Main {
	public static void main(String args[]) {
		Huisdier dier1 = new Kat("fritz");
		Huisdier dier2 = new Huisdier("bob");
		Hamster dier3 = new Hamster("bob");
		Hond dier4 = new Hond("puck");
		
		Dierenwinkel winkel = new Dierenwinkel();
		winkel.voegToe(dier1);
		winkel.voegToe(dier2);
		winkel.voegToe(dier3);
		winkel.voegToe(dier4);
		winkel.printOverzicht();
	}
}
