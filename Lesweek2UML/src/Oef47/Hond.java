package Oef47;

public class Hond extends Huisdier {

	public Hond(String naam) {
		super(naam);
	}
	
	public void maakGeluid() {
		System.out.println("Woef woef woef");
	}
	
	public void kwispel() {
		System.out.println("kwispel, kwispel, kwispel");
	}
	
}
