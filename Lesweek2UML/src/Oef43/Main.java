package Oef43;

public class Main {
	public static void main(String args[]) {
		//Maak datums aan voor student.
		Datum geboorteDatumStudent1 = new Datum(18, 04, 1992);
		Datum startDatumStudent1 = new Datum(22, 05, 2014);
		Datum eindDatumStudent1 = new Datum(8, 07, 2014);
		
		//Maak een studiepgramma aan met start en einddatum
		Studieprogramma programmaStudent1 = new Studieprogramma(startDatumStudent1, eindDatumStudent1);
		
		//Vakken aanmaken
		Vak vak1 = new Vak("Nederlands");
		Vak vak2 = new Vak("Duits");
		Vak vak3 = new Vak("Biologie");
		Vak vak4 = new Vak("Geschiedenis");
		Vak vak5 = new Vak("Natuurkunde");
		
		//Vakken toevoegen aan het studie programma van student 1
		programmaStudent1.voegVakToe(vak1);
		programmaStudent1.voegVakToe(vak2);
		programmaStudent1.voegVakToe(vak3);
		programmaStudent1.voegVakToe(vak4);
		programmaStudent1.voegVakToe(vak5);
		
		Student student1 = new Student("Nick", 2071330, geboorteDatumStudent1, programmaStudent1);
		
		//Print informatie van de student
		programmaStudent1.printStudieprogramma();
		
	}
}
