package Oef43;

public class Student {
	private String naam;
	private int nr;
	private Datum geboorteDatum;
	private Studieprogramma programma;
	
	public Student(String naam, int nummer, Datum geboorteDatum, Studieprogramma programma) {
		this.naam = naam;
		this.nr = nummer;
		this.geboorteDatum = geboorteDatum;
		this.programma = programma;
	}
	
	public String getNaam() {
		return naam;
	}
	
	public int getNr() {
		return nr;
	}
	
}
