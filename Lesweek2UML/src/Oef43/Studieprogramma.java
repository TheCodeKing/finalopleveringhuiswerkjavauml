package Oef43;

import java.util.ArrayList;

public class Studieprogramma {
	private Datum startDatum;
	private Datum eindDatum;
	private ArrayList<Vak> vakLijst;
	
	public Studieprogramma(Datum startDatum, Datum eindDatum) {
		this.startDatum = new Datum(22, 05, 2014);
		this.eindDatum = new Datum(8, 07, 2014);
		vakLijst = new ArrayList();
	}
	
	public void voegVakToe(Vak vak) {
		vakLijst.add(vak);
	}
	
	public void printStudieprogramma() {
		System.out.println("Het studierooster bestaat uit:");
		for(Vak vak : vakLijst) {
			System.out.println(vak);
		}
		System.out.print("De startdatum van het rooster: ");
		System.out.println(this.startDatum.getDag() + " " + this.startDatum.getMaand() + " " + this.startDatum.getJaar());
		System.out.print("De einddatum van het rooster: ");
		System.out.println(this.eindDatum.getDag() + " " + this.eindDatum.getMaand() + " " + this.eindDatum.getJaar());
	}
	
}
