package Oef45;

public class Huisdier {
	private String naam;
	
	public Huisdier(String naam) {
		this.naam = naam;
	}
	
	public String getNaam (String naam) {
		return naam;
	}
	
	public void maakGeluid() {
		System.out.println("Het dier maakt het volgende geluid: ");
	}
}
