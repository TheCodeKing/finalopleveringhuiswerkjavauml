package Oef45;

public class Main {
	public static void main(String args[]) {
		//a
		//Huisdier dier1 = new Hamster();
		
		//b
		Huisdier dier2 = new Kat("Fritz");
		
		//c
		Huisdier dier3 = new Huisdier("Pluk");
		
		//d
		//Kat kat = new Huisdier("Felix");
		
		//e
		Hamster hamster = new Hamster("Knibbel");
		//System.out.println(hamster.getNaam());
		
		//f
		Huisdier dier4 = new Hond("Bobbie");
		dier4.maakGeluid();
		
		//g
		Huisdier dier5 = new Kat("Droppie");
		//dier5.spin();
		
		//h
		Kat kat = new Kat("Zuus");
		kat.spin();
		
		//i
		//Hamster hamster1 = new Hond("Gijs");
		//hamster.kwispel();
		
		//j
		Huisdier dier = new Hond("Tarzan");
		((Hond) dier).kwispel();
		
		
	}
}
