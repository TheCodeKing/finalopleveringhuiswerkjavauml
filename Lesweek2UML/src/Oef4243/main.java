package Oef4243;

public class main {
	public static void main(String[] args) {
		Klant klant_1 = new Klant("Maarten", 19024508);
		Product product_1 = new Product("Maarten's beker", 34.90);
		Product product_2 = new Product("Jan wintermans handdoek", 39.90);
		Product product_3 = new Product("Edem handgranaten", 34.90);
		
		Bestelling bestelling = new Bestelling(klant_1, product_1);
		bestelling.addProduct(product_2);
		bestelling.addProduct(product_3);
		
		bestelling.print();
	}
}
