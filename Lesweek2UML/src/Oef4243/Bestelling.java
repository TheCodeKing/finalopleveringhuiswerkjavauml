package Oef4243;
import java.util.ArrayList;


public class Bestelling {
	private Klant klant;
	private ArrayList<Product> bestellijst = new ArrayList<Product>();
	
	public Bestelling(Klant klant, Product product) {
		this.klant = klant;
		bestellijst.add(product);
	}
	
	public void addProduct(Product product) {
		bestellijst.add(product);
	}
	
	public void print() {
		for(Product product: bestellijst) {
			System.out.println(product);
		}
	}
}
