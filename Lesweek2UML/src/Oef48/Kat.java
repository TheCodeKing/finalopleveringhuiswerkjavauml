package Oef48;

public class Kat extends Huisdier {
	
	public Kat(String naam) {
		super(naam);
	}
	
	public void maakGeluid() {
		System.out.println("De kat miauwtttt");
	}
	
	public void spin() {
		System.out.println("pur pur pur pur");
	}
}
