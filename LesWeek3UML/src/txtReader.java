import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class txtReader {
	public txtReader(){
		
	}
	
	public void readTxt(String src) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(src));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String regel;
		try {
			while((regel = in.readLine()) != null) {
				System.out.println(regel);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readPersonenTxt(String src) {
		System.out.println("---------------------------");		
		System.out.println("readPersonenTxt() :");
		System.out.println("---------------------------");
		try {
			File fXmlFile = new File(src);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("persoon");
			
			for (int i = 0; i < nList.getLength(); i++) {

				Node nNode = nList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element e = (Element) nNode;
					System.out.println(i + 1 + ".");
					System.out.println("naam: " + e.getElementsByTagName("naam").item(0).getTextContent());
					System.out.println("adres: " + e.getElementsByTagName("adres").item(0).getTextContent());
					System.out.println("woonplaats: " + e.getElementsByTagName("woonplaats").item(0).getTextContent());
					System.out.println("---------------------------");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}