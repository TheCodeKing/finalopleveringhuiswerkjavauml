import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class objectReader {
	public objectReader(){
		
	}
	
	public void readObject(String src){
		Persoon persoon = null;
		try {ObjectInputStream in = new ObjectInputStream(new FileInputStream(src));
			persoon = (Persoon) in.readObject();
			in.close();
		} catch(ClassNotFoundException e) {
			System.out.println("Onbekende klasse bij inlezen bestand " + src);
		} catch(IOException e) {
			System.out.println("Fout bij openen bestand " + src);
			e.printStackTrace();
		}
		System.out.println("Naam: " + persoon.getNaam());
		System.out.println("Adres: " + persoon.getAdres());
		System.out.println("Woonplaats: " + persoon.getWoonplaats());
	}
	
	public void readPersonen(String src) {
		ArrayList<Persoon> personen = null;
		try {ObjectInputStream in = new ObjectInputStream(new FileInputStream(src));
		personen = (ArrayList<Persoon>) in.readObject();
		in.close();
		} catch(ClassNotFoundException e) {
			System.out.println("Onbekende klasse bij inlezen bestand " + src);
		} catch(IOException e) {
			System.out.println("Fout bij openen bestand " + src);
			e.printStackTrace();
		}
		for(int i = 0; i < personen.size(); i++){
			Persoon persoon = personen.get(i);
			System.out.println(i + 1 + ".");
			System.out.println("Naam: " + persoon.getNaam());
			System.out.println("Adres: " + persoon.getAdres());
			System.out.println("Woonplaats: " + persoon.getWoonplaats());
			System.out.println("---------------------------------");
		}
	}
}