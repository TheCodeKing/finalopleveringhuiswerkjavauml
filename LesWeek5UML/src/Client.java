
public class Client {
	public void someMethod() {
		//Create an instance of TaskClass
		TaskClass task = new TaskClass();
		
		//Create a thread
		Thread thread = new Thread(task);
		
		//Start a thread
		thread.start();
	}
}
