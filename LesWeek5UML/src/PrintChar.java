
public class PrintChar implements Runnable {
	private char letter;
	private int number;
	
	public PrintChar(char letter, int number) {
		this.letter = letter;
		this.number = number;
	}
	
	public void run() {
		for(int i = 0; i <= number; i++) {
			System.out.print("letter: " + letter);
		}
	}
}
