
public class PrintNum implements Runnable {
	private int number;
	
	public PrintNum(int number) {
		this.number = number;
	}
	
	public void run() {
		for(int i = 0; i <= number; i++) {
			System.out.print("number: " + i);
		}
	}
}
